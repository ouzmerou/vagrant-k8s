%title: Vagrant et Kubernetes
%author: xavki

# Kubernetes avec Vagrant

<br>
Objectifs :

* apprendre à utiliser vagrant : cf mise en place d'un mesh

* outil pour provisionner et installer des VM sur virtualbox et d'autres provider (vmware....)

* mode avancé = ansible... (pre-provisionning)

* à base de ruby

<br>
prérequis :

* installer virtualbox

```
apt-get install virtualbox
```

* installer vagrant

```
wget https://releases.hashicorp.com/vagrant/2.2.5/vagrant_2.2.5_x86_64.deb
dpkg -i vagrant_2.2.5_x86_64.deb
```


---------------------------------------------------------------------------

# Vagrantfile

<br>
* description des machines virtuelles

<br>
* provisionnement d'une VM

```
Vagrant.configure("2") do |config|
  # master server
  config.vm.define "kmaster" do |kmaster|
    kmaster.vm.box = "debian/stretch64"
    kmaster.vm.hostname = "kmaster"
    kmaster.vm.box_url = "debian/stretch64"
    kmaster.vm.network :private_network, ip: "192.168.100.10"
    kmaster.vm.provider :virtualbox do |v|
      v.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
      v.customize ["modifyvm", :id, "--memory", 2048]
      v.customize ["modifyvm", :id, "--name", "kmaster"]
      v.customize ["modifyvm", :id, "--cpus", "2"]
    end
  end
end
```

